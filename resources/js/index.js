"use strict";

/* 
    Autor: Alberto Caro
    Fecha creación: 29/10/2017
    Última modificación: 29/10/2017
    Versión: 1.00
*/

var links = [
    "frames/calculadora.html",
    "frames/agenda.html"
];
var iframe = document.querySelectorAll("iframe")[1]; // Seleccionamos el 2do iframe;

/**
 * Cambia el source del iframe por el del array con el index especificado
 * @param {number} num 
 */
function cambiaSrc(num) {
    iframe.src = links[num.data];
}


window.addEventListener("message", function(e) {
    cambiaSrc(e);
});
