"use strict";

/* 
    Autor: Alberto Caro
    Fecha creación: 29/10/2017
    Última modificación: 05/11/2017
    Versión: 0.01
*/

// Elementos
var flechas         = document.querySelectorAll("#flechas button"),
    numPagina       = document.querySelector("#numPagina"),
    inputEntrada    = document.querySelector("input[type='number']"),
    botonVer        = document.querySelector("#btnVer"),
    nombre          = document.querySelectorAll("#datos input")[0],
    apellidos       = document.querySelectorAll("#datos input")[1],
    telefono        = document.querySelectorAll("#datos input")[2],
    nacimiento      = document.querySelectorAll("#datos input")[3],
    botonNuevo      = document.querySelector("#nuevo"),
    botonCancelar   = document.querySelector("#cancelar"),
    botonGuardar    = document.querySelector("#guardar"),
    botonBorrar     = document.querySelector("#borrar"),
    modal           = document.querySelector("#errorReg"),
    campoError      = document.querySelectorAll(".alerta"),
    telfError       = document.querySelector("#alerta-span"),
    fondoModal      = document.querySelector(".modal-background"),
    tabla           = document.querySelector("tbody"),
    tituloTabla     = document.querySelector("#tituloResumen"),
    cantidadRegis   = document.querySelector("#cantidad"),
    selector        = document.querySelector("select"),
    inputBusqueda   = document.querySelector("#busqueda"),
    botonBusqueda   = document.querySelector("#btnBuscar"),
    cierraModal     = document.querySelector(".modal-close");

// Evitar uso de numeros negativos en el input de numero pagina
inputEntrada.onkeydown = function(e) {
    if(!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58) 
      || e.keyCode == 8)) {
        return false;
    }
};

// Array de personas con 4 creadas
var personas = [
    {
        nombre: "Luis",
        apellidos: "Lopez Pérez",
        telefono: "959667788",
        nacimiento: "1987-01-12"
    },
    {
        nombre: "Arturo",
        apellidos: "Fermín López",
        telefono: "959347586",
        nacimiento: "1990-04-06"
    },
    {
        nombre: "Jesús",
        apellidos: "Gutiérrez Ferreira",
        telefono: "654647857",
        nacimiento: "1973-10-20"
    },
    {
        nombre: "César",
        apellidos: "Fort Lagos",
        telefono: "654867192",
        nacimiento: "2000-03-19"
    }
];

var nuevoContacto = false;
var posicion = 1;
const TOTAL_REGISTROS = 100;

/**
 * Actualiza los datos que se muestran
 * @param {number} indice Posición del array
 */
function mostrarDatos(indice) {
    let pos = personas.length > 0 ? indice - 1 : -1; // El array comienza por 0, por lo que restamos uno.
    if (comprobarPosicion(pos)) {
        posicion = indice;
        let total = personas.length;
        let persona = personas[pos];
        // Actualizamos el numero de pagina
        numPagina.innerHTML = `${posicion} de ${total}`;
        // Actualizamos los datos mostrados
        nombre.value = persona.nombre;
        apellidos.value = persona.apellidos;
        telefono.value = persona.telefono;
        nacimiento.value = persona.nacimiento;
    }
    actualizaResumen();
    ocultaErrores();
}

/**
 * Comprueba si existe una posición de un array
 * @param {number} pos Posición a comprobar
 */
function comprobarPosicion(pos) {
    if(pos === -1) return false;
    if (personas[pos] !== void 0) { // void 0 es igual que undefined pero
        return true;                // con mayor compatibilidad
    } else {
        mostrarError();
        return false;
    }
}

/**
 * Activa el modal
 */
function mostrarError() {
    modal.classList.add("is-active");
}

/**
 * Desactiva los botones
 */
function desactivaMenu() {
    flechas.forEach(function(flecha) {
        flecha.setAttribute("disabled", "true");
    });
    botonVer.setAttribute("disabled", "true");
    botonBorrar.setAttribute("disabled", "true");
}

/**
 * Activa los botones
 */
function activaMenu() {
    flechas.forEach(function(flecha) {
        flecha.removeAttribute("disabled");
    });
    botonVer.removeAttribute("disabled");
    botonBorrar.removeAttribute("disabled");
}

/**
 * Limpia todos los inputs
 */
function limpiaCampos() {
    nombre.value = "";
    apellidos.value = "";
    telefono.value = "";
    nacimiento.value = "";
}

/**
 * Pone la primera letra de cada palabra en mayuscula
 * @param {string} cadena 
 */
function capitalizaCadena(cadena){
    return cadena.replace(/\b\w/g, function(el){return el.toUpperCase();});
}

/**
 * Comprueba los datos y los guarda en el array
 */
function guardaDatos() {
    ocultaErrores();
    if(compruebaDatos()) {
        let persona = {};
        persona["nombre"] = capitalizaCadena(nombre.value);
        persona["apellidos"] = capitalizaCadena(apellidos.value);
        persona["telefono"] = telefono.value;
        persona["nacimiento"] = nacimiento.value;
        if(nuevoContacto) {
            personas.push(persona);
            if (posicion === 0) posicion = 1;
            nuevoContacto = false;
        } else {
            if(posicion > 0) {
                personas[posicion - 1] = persona;
            } else {
                mostrarError();
            }
        }
        return true;
    } else {
        return false;
    }
}

/**
 * Comprueba los datos introducidos
 */
function compruebaDatos() {
    let correcto = true;
    if(nombre.value === "") {
        muestraCampoError(0);
        correcto = false;
    }
    if(apellidos.value === "") {
        muestraCampoError(1);
        correcto = false;
    }
    if(telefono.value === "" || !/^[9876]\d{8}$/.test(telefono.value)) {
        if(telefono.value === "") {
            muestraCampoError(2);
        } else {
            muestraCampoError(2);
        }
        correcto = false;
    }
    if(nacimiento.value === "") {
        muestraCampoError(3);
        correcto = false;
    }
    return correcto; // Ningun campo esta vacio
}

/**
 * Muestra un div con un error junto al input con el indice introducido
 * @param {number} campo 
 */
function muestraCampoError(campo) {
    let notificacion = campoError[campo];
    if(campo === 2) {
        if(telefono.value === "") {
            telfError.innerHTML = "obligatorio";
        } else {
            telfError.innerHTML = "invalido";
        }
    }
    notificacion.style.display = "block";
}

/**
 * Oculta todos los errores
 */
function ocultaErrores() {
    campoError.forEach(function(val) {
        val.style.display = "none";
    });
}

/**
 * Actualiza el resumen
 */
function actualizaResumen() {
    tituloTabla.innerHTML = "RESUMEN AGENDA";
    cantidadRegis.style.display = "block";
    cantidadRegis.innerHTML = `${personas.length} de ${TOTAL_REGISTROS} registros almacenados.`;
    // Borramos todas las filas
    while(tabla.rows.length > 0) {
        tabla.deleteRow(0);
    }
    // Agregamos las filas nuevas
    personas.forEach(function(val, index) {
        // Creamos la fila
        let nuevaFila = tabla.insertRow(tabla.rows.length);
        // Creamos la celda de la posicion
        let nuevaCelda = nuevaFila.insertCell(0);
        let contenido = document.createTextNode(`${index + 1}`);
        nuevaCelda.appendChild(contenido);
        // Creamos las celdas de cada propiedad
        for(const dato in val) {
            let nuevaCelda = nuevaFila.insertCell();
            let contenido;
            if(dato === "nacimiento") {
                contenido = document.createTextNode(`${new Date(val[dato]).toLocaleDateString("es-ES",{year: "numeric", month: "2-digit", day: "2-digit"})}`);
            } else {
                contenido = document.createTextNode(`${val[dato]}`);
            }
            nuevaCelda.appendChild(contenido);
        }
    });
}

/**
 * Muestra los resultados de la búsqueda
 * @param {array} resultados 
 */
function muestraResultados(resultados) {
    tituloTabla.innerHTML = "RESULTADOS BUSQUEDA";
    cantidadRegis.style.display = "none";
    // Borramos todas las filas
    while(tabla.rows.length > 0) {
        tabla.deleteRow(0);
    }
    // Agregamos las filas nuevas
    resultados.forEach(function(val) {
        // Creamos la fila
        let nuevaFila = tabla.insertRow(tabla.rows.length);
        // Creamos las celdas de cada propiedad
        for(const dato in val) {
            let nuevaCelda = nuevaFila.insertCell();
            let contenido = document.createTextNode(`${val[dato]}`);
            nuevaCelda.appendChild(contenido);
        }
    });
}

/**
 * Realiza una busqueda y muestra los resultados
 * @param {string} dato Cadena a buscar 
 * @param {string} propiedad Tipo a buscar (Nacimiento, Nombre, etc...)
 */
function buscar(dato, propiedad) {
    let resultados = [];
    personas.forEach(function(val, index) {
        let expresion = new RegExp(dato, "i");
        let valor = val[propiedad];
        let valorSinTilde = val[propiedad].normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        if(expresion.test(valor) || expresion.test(valorSinTilde)) {
            let persona = {};
            persona["pos"] = index + 1;
            for (var i in val) persona[i] = val[i];
            resultados.push(persona); 
        }
    });
    muestraResultados(resultados);
}

// Listenners
cierraModal.addEventListener("click", function(){
    modal.classList.remove("is-active");
});

fondoModal.addEventListener("click", function(){
    modal.classList.remove("is-active");
});

flechas.forEach(function(val) {
    let flecha = val.dataset.valor; // Identificamos que flecha es
    val.addEventListener("click", function() {
        let pos = posicion;
        switch(flecha) {
            case "inicio":
                mostrarDatos(1);
                break;
            case "siguiente":
                if (pos < personas.length) mostrarDatos(++pos);
                break;
            case "atras":
                if (pos > 1) mostrarDatos(--pos);
                break;
            case "fin":
                mostrarDatos(personas.length);
                break;
            default:
                mostrarError();
                break;
        }
        inputEntrada.value = "";
    });
});

botonVer.addEventListener("click", function() {
    let entrada = inputEntrada.value;
    if (personas.length <= 0 || entrada === "0") {
        mostrarError();
        return;
    }
    mostrarDatos(entrada);
});

botonNuevo.addEventListener("click", function() {
    desactivaMenu();
    limpiaCampos();
    ocultaErrores();
    let total = personas.length;
    let pos = Number(total + 1);
    numPagina.innerHTML = `${pos} de ${pos}`; // Aumentamos en 1 la posicion desde el final
    this.style.display = "none";
    botonCancelar.removeAttribute("style");
    nuevoContacto = true;
});

botonCancelar.addEventListener("click", function() {
    activaMenu();
    mostrarDatos(posicion); // Mostramos los datos en los que estabamos
    this.style.display = "none";
    botonNuevo.removeAttribute("style");
    if(personas.length <= 0) numPagina.innerHTML = "0 de 0";
    nuevoContacto = false;
});

botonGuardar.addEventListener("click", function() {
    if(guardaDatos()) {
        activaMenu();
        mostrarDatos(posicion);
        botonNuevo.removeAttribute("style");
        botonCancelar.style.display = "none";
    }
});

botonBorrar.addEventListener("click", function() {
    let pos = Number(posicion - 1);
    personas.splice(pos,1);
    let total = personas.length;
    if (total === 0) {
        limpiaCampos();
        posicion = 0;
        numPagina.innerHTML = `${posicion} de ${total}`;
        actualizaResumen();
        return;
    }
    if (posicion > personas.length) posicion = personas.length;
    mostrarDatos(posicion);
});

selector.addEventListener("change", function() {
    let opcion = this.value;
    if (opcion === "Teléfono") {
        inputBusqueda.type = "tel";
    } else if (opcion === "Nacimiento") {
        inputBusqueda.type = "date";
    } else {
        inputBusqueda.type = "text";
    }
    inputBusqueda.value = "";
});

botonBusqueda.addEventListener("click", function() {
    let atributo = selector.value.toLocaleLowerCase();
    if(atributo === "teléfono") atributo = "telefono";
    buscar(inputBusqueda.value, atributo);
});