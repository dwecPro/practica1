"use strict";

/* 
    Autor: Alberto Caro
    Fecha creación: 28/10/2017
    Última modificación: 01/11/2017
    Versión: 1.00
*/

// Elementos
var pantalla    = document.querySelector(".pantalla"),
    otros       = document.querySelectorAll(".otros div"),
    numeros     = document.querySelectorAll(".numeros div"),
    operadores  = document.querySelectorAll(".operadores div");

// Expresion regular y otros datos
var expresion = /(-?\d+(?:[.,]\d+)?)([/%*+\-x√])?(-?\d+(?:[.,]\d+)?)*/g;
const TOTAL_NUMEROS_PANTALLA = 15;
const DECIMALES = 3;
var resetNumero = false;

// Funciones

/**
 * Lee los datos de la pantalla y escribe la solución de la operación
 */
function resuelveOperacion() {
    // Reemplazamos la x por * para que funcione eval
    pantalla.innerHTML = pantalla.innerHTML.replace(/x/g,"*"); 
    let datos = obtenerDatos();
    // Sustituimos la pantalla con el resultado. Usamos regex para evitar parametros peligrosos en el eval.
    if(datos) {
        let resultado;
        if(datos.operando2 && datos.operando2.charAt(0) === "0" && datos.operando2.indexOf(".") === -1) {
            resultado = eval(ajustaCadena(datos.operando1, datos.operador, Number(datos.operando2)));
        } else {
            resultado = eval(datos.operacion);
        }
        if(isNaN(resultado)) {
            pantalla.innerHTML = "Error";
        } else {
            pantalla.innerHTML = !isFinite(resultado) ? "Infinito" : ajustaCadena(redondeaNumero(resultado)); 
        }
    }
}

/**
 * Lee los datos y calcula el porcentaje
 */
function calculaPorcentaje() {
    let datos = obtenerDatos();
    if(datos && !isNaN(datos.ultimoCaracter) && datos.operando2 && datos.operador === "x") {
        let resultado = redondeaNumero(datos.operando1 * datos.operando2 * 0.01);
        pantalla.innerHTML = ajustaCadena(resultado);
    } else {
        pantalla.innerHTML = ajustaCadena("Error");
    }
}

/**
 * Lee los datos y escribe el inverso del ultimo operando
 */
function partidoPorUno() {
    let datos = obtenerDatos();
    if(datos && !isNaN(datos.ultimoCaracter)) {
        if(datos.operando2) {
            // Si el operando es 0, el resultado es infinito.
            if(Number(datos.operando2) === 0) {
                pantalla.innerHTML = "Infinito"; 
                return;
            }
            let resultado = redondeaNumero(1 / datos.operando2);
            pantalla.innerHTML = ajustaCadena(datos.operando1, datos.operador, resultado);
        } else {
            // Si el operando es 0, el resultado es infinito.
            if(Number(datos.operando1) === 0) {
                pantalla.innerHTML = "Infinito"; 
                return;
            }
            let resultado = redondeaNumero(1 / datos.operando1);
            pantalla.innerHTML = ajustaCadena(resultado);
        }
    }
}

/**
 * Lee los datos y devuelve la raíz cuadrada del ultimo operando
 */
function raizCuadrada() {
    let datos = obtenerDatos();
    //Comprobamos que el ultimo caracter no sea un numero
    if(datos && !isNaN(datos.ultimoCaracter)) {
        if(datos.operando2) {
            let resultado = redondeaNumero(Math.sqrt(datos.operando2));
            // Si el numero era negativo...
            if (isNaN(resultado)) {
                pantalla.innerHTML = "Error";
                return;
            }
            pantalla.innerHTML = ajustaCadena(datos.operando1, datos.operador, resultado);
        } else {
            let resultado = redondeaNumero(Math.sqrt(datos.operando1));
            // Si el numero era negativo...
            if (isNaN(resultado)) {
                pantalla.innerHTML = "Error";
                return;
            }
            pantalla.innerHTML = ajustaCadena(resultado);
        }
    }
}

/**
 * Lee los datos y devuelve el inverso del ultimo operando
 */
function masMenos() {
    let datos = obtenerDatos();
    if(datos && !isNaN(datos.ultimoCaracter)) {
        if(datos.operando2) { 
            // Si el operador es + o - cambiamos el operador de signo
            if(/[+-]/.test(datos.operador)) {
                if(datos.operador === "-") {
                    pantalla.innerHTML = ajustaCadena(datos.operando1, "+", datos.operando2);
                    return;
                } else {
                    pantalla.innerHTML = ajustaCadena(datos.operando1, "-", datos.operando2);
                    return;
                }
            }
            let resultado = datos.operando2 * -1;
            pantalla.innerHTML = ajustaCadena(datos.operando1, datos.operador, resultado);
        } else {
            let resultado = datos.operando1 * -1;
            pantalla.innerHTML = ajustaCadena(resultado);
        }
    }
}

/**
 * Elimina el ultimo caracter de la pantalla
 */
function eliminaUno() {
    if (pantalla.innerHTML === "Error" || pantalla.innerHTML === "Infinito") {
        pantalla.innerHTML = "0"; 
        return;
    } 
    // Eliminamos el ultimo caracter de la pantalla
    pantalla.innerHTML = pantalla.innerHTML.substring(0, pantalla.innerHTML.length - 1);
    let datos = obtenerDatos();
    // Obligamos a que se adapte a la expresión regular
    if(datos) {
        pantalla.innerHTML = ajustaCadena(datos.operacion);
    }

    // Si la pantalla esta vacia, la sustituimos por 0
    if(pantalla.innerHTML === "" || !datos) pantalla.innerHTML = "0";
}

/**
 * Lee los datos por la pantalla y los devuelve
 * @returns {object} Datos de la pantalla
 */
function obtenerDatos() {
    const regex = expresion.exec(pantalla.innerHTML);
    // Reseteamos el objeto regex para que a la proxima comience desde el principio
    expresion.lastIndex = 0; 

    if(!regex) {
        return null;
    }

    // Antes de reemplazar las comas por puntos, comprobamos si existe el operando
    let datos = {
        "pantalla": pantalla.innerHTML,
        "operacion": regex[0].replace(/,/g,"."),
        "operando1": typeof regex[1] === "undefined" ? undefined : regex[1].replace(/,/g,"."),
        "operando2": typeof regex[3] === "undefined" ? undefined : regex[3].replace(/,/g,"."),
        "operador": regex[2],
        "ultimoCaracter": pantalla.innerHTML.charAt(pantalla.innerHTML.length -1)
    };
    return datos;
}

/**
 * Une los argumentos que se le pase y reemplaza los puntos por comas
 * @returns {string} Parametros unidos, con el punto sustituido por la coma
 */
function ajustaCadena() {
    // Convertimos arguments a array
    const args = Array.from(arguments); 
    return args.join("").replace(/\./g,",");
}

/**
 * Redondea el numero a lo específicado
 * @param {number} num - Numero a redondear 
 * @returns {number} - Numero redondeado
 */
function redondeaNumero(num) {
    let redondeo = 1;
    for(let i = 0; i < DECIMALES; i++) {
        redondeo *= 10;
    }
    return Math.round(num * redondeo) / redondeo;
}

// Listenners

//Listenners de numeros
numeros.forEach(function(val) {
    val.addEventListener("click", function() {
        const tecla = val.dataset.valor;
        const textoPantalla = pantalla.innerHTML;
        let datos = obtenerDatos();
        if(textoPantalla.length < TOTAL_NUMEROS_PANTALLA) {
            if(tecla === "coma") {
                if(datos && !(isNaN(datos.ultimoCaracter))) {
                    // Si el operando no tiene coma, se la agregamos
                    if(datos.operando2) {
                        if(!/\./.test(datos.operando2)) pantalla.innerHTML += ",";
                    } else {
                        if(!/\./.test(datos.operando1)) pantalla.innerHTML += ",";
                    }
                }
            } else {
                if(textoPantalla === "Error" || textoPantalla === "Infinito" || textoPantalla === "0" || resetNumero) pantalla.innerHTML = ""; // Limpiamos el mensajes
                pantalla.innerHTML += tecla;
            }
            resetNumero = false;
        }
    });
});

//Listenners de otros
otros.forEach(function(val) {
    val.addEventListener("click", function() {
        const tecla = val.dataset.valor;
        let datos = obtenerDatos();
        resetNumero = false;
        if (tecla === "borrar") {
            eliminaUno();
        } else if (tecla === "C") {
            pantalla.innerHTML = "0";
        } else if (tecla === "CE") {
            if(datos && datos.operando2) { //Borramos el segundo operando solo si existe
                // Escribimos solo el primer operando y el operador
                pantalla.innerHTML = ajustaCadena(datos.operando1, datos.operador); 
            } else {
                // En caso de que no haya segundo operando, borramos todo
                pantalla.innerHTML = "0";
            }
        } else if(tecla === "masmenos") {
            masMenos();
        } else if(tecla === "raiz") {
            raizCuadrada();
        }
    });
});

// Listenners de operadores
operadores.forEach(function(val) {
    val.addEventListener("click", function() {
        const tecla = val.dataset.valor;
        let datos = obtenerDatos();
        if(pantalla.innerHTML === "0") return;
        if(datos) {
            if (tecla === "1/x") {
                partidoPorUno();
                resetNumero = false;
            } else if(tecla === "%") {
                calculaPorcentaje();
                resetNumero = false;
            } else if(tecla === "=") {
                resuelveOperacion();
                resetNumero = true;
            } else if(!datos.operando2) { 
                let ultimoCaracter = datos.pantalla.charAt(datos.operando1.length);
                // Comprobamos que no haya una coma sin numero
                if (ultimoCaracter !== ",") {
                    if(!datos.operador) {
                        pantalla.innerHTML += tecla;
                    } else { 
                        // Si ya hay un operador lo sustituimos
                        // Eliminamos el ultimo caracter (el operador)
                        pantalla.innerHTML = datos.pantalla.substring(0, datos.pantalla.length - 1);
                        pantalla.innerHTML += tecla;
                    }
                }
                resetNumero = false;
            } else {
                resuelveOperacion();
                pantalla.innerHTML += tecla;
            }
        }
    });
});
