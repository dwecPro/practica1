"use strict";

/* 
    Autor: Alberto Caro
    Fecha creación: 29/10/2017
    Última modificación: 29/10/2017
    Versión: 1.00
*/

$(".opcion1").on("click", function() {
    parent.postMessage("0", "*");
});

$(".opcion2").on("click", function() {
    parent.postMessage("1", "*");
});

$(".opcion1, .opcion2").on("mouseenter", function() {
    $(this).find(".animated").addClass("tada").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", 
        function() {
            $(this).removeClass("tada");
        });
});
